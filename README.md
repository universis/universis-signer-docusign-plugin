# universis-signer-docusign

![Universis Project](src/main/resources/universis_logo_128_color.png)

An extender of [universis signer](https://gitlab.com/universis/universis-signer) for implementing DocuSign cloud signing.

## Installation

1. Download [Universis signer plugin for docusign](https://api.universis.io/releases/universis-signer-docusign-client/latest/universis-signer-docusign-client.zip)

2. Extract universis-signer-docusign-client.zip and copy content (universis-signer-docusign-client/*) to universis signer installation directory.

3. Restart universis signer

**Important note: This plugin is available for universis-signer@1.8.0 or greater**

## Manual installation

Build module 

    mvn package

Copy `target/universis-signer-docusign.jar` to installation directory of universis signer

Edit `extras/service.properties` of universis signer in order to set `keyStore` and `storeType` properties

    storeType=REMOTE
    keyStore=https://dsa.example.com/

Add `extras/routes.xml` to override universis signer routes

    <routes>
        <route>
            <path>/openapi/schema.json</path>
            <plugin>universis-signer-docusign.jar</plugin>
            <className>org.universis.signer.docusign.DocuSignOpenapiHandler</className>
        </route>
        <route>
            <path>/slots</path>
            <className>org.universis.signer.NotImplementedHandler</className>
        </route>
        <route>
            <path>/keystore/certs</path>
            <plugin>universis-signer-docusign.jar</plugin>
            <className>org.universis.signer.docusign.DocuSignKeyStoreHandler</className>
        </route>
        <route>
            <path>/sign</path>
            <plugin>universis-signer-docusign.jar</plugin>
            <className>org.universis.signer.docusign.DocuSignSignerHandler</className>
        </route>
        <route>
            <path>/requestCode</path>
            <className>org.universis.signer.docusign.DocuSignRequestCodeHandler</className>
        </route>
    </routes>
