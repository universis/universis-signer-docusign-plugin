package org.universis.signer.docusign;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DocuSignRequestCodeResult implements Serializable {
    @JsonProperty("Outcome")
    public int outcome;

    @JsonProperty("Description")
    public String description;
}
