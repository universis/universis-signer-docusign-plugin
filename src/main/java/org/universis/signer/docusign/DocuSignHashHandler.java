package org.universis.signer.docusign;

import com.itextpdf.text.pdf.security.DigestAlgorithms;
import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.router.RouterNanoHTTPD;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.universis.signer.*;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.MessageDigest;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

public class DocuSignHashHandler implements RouterNanoHTTPD.UriResponder {
    private static final Logger log = LogManager.getLogger(SignerHandler.class);
    public HashMap<String, String> files = null;

    @Override
    public NanoHTTPD.Response get(RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        return new MethodNotAllowedHandler().get(uriResource, map, ihttpSession);
    }

    @Override
    public NanoHTTPD.Response put(RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        return new MethodNotAllowedHandler().get(uriResource, map, ihttpSession);
    }

    @Override
    public NanoHTTPD.Response post(RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        try {
            // parse files
            this.files = new HashMap<>();
            ihttpSession.parseBody(this.files);
            //noinspection StatementWithEmptyBody
            for (@SuppressWarnings("UnusedAssignment") String ignored : this.files.values()) {
                // read
            }
            // get pdf file
            String file = null;
            for (String key : this.files.keySet()) {
                if (key.startsWith("file")) {
                    file = this.files.get(key);
                    break;
                }
            }
            assert file != null;
            byte[] bytes = FileUtils.readFileToByteArray(new File(file));
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
            byte[] messageHash = DigestAlgorithms.digest(new ByteArrayInputStream(bytes), messageDigest);
            String value = Base64.getEncoder().encodeToString(messageHash);
            DocuSignHashHandlerResult result = new DocuSignHashHandlerResult();
            result.value = new String[]{
                    value
            };
            NanoHTTPD.Response res = new JsonResponseHandler(result).get(uriResource, map, ihttpSession);
            CorsHandler.enable(ihttpSession, res);
            return res;
        } catch (NanoHTTPD.ResponseException | IOException | GeneralSecurityException e) {
            log.error(ExceptionUtils.getStackTrace(e));
            return new ServerErrorHandler(e).get(uriResource, map, ihttpSession);
        }
    }

    @Override
    public NanoHTTPD.Response delete(RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        return new MethodNotAllowedHandler().get(uriResource, map, ihttpSession);
    }

    @Override
    public NanoHTTPD.Response other(String s, RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        return new CorsHandler().other(s, uriResource, map, ihttpSession);
    }
}
