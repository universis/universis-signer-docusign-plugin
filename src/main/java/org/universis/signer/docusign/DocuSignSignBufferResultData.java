package org.universis.signer.docusign;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class DocuSignSignBufferResultData {
    @JsonProperty("Signature")
    public String signature;

}
