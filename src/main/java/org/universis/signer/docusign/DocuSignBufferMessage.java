package org.universis.signer.docusign;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DocuSignBufferMessage {
    @JsonProperty("Username")
    public String username;

    @JsonProperty("Password")
    public String password;

    @JsonProperty("SignPassword")
    public String otp;

    @JsonProperty("BufferToSign")
    public ArrayList<String> buffer = new ArrayList<>();

    @JsonProperty("Flags")
    public String flags;

}
