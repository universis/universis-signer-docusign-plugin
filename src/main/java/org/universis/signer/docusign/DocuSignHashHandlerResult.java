package org.universis.signer.docusign;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.io.Serializable;

@JsonSerialize
public class DocuSignHashHandlerResult implements Serializable {

    @JsonProperty
    public String[] value;

}