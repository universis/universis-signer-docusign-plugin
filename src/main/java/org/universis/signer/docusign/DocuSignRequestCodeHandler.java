package org.universis.signer.docusign;

import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.router.RouterNanoHTTPD;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.universis.signer.*;

import java.util.Map;

public class DocuSignRequestCodeHandler implements RouterNanoHTTPD.UriResponder {
    private static final Logger log = LogManager.getLogger(KeyStoreHandler.class);
    @Override
    public NanoHTTPD.Response post(RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        // get authorization header
        String[] usernamePassword;
        log.debug("get authorization header");
        String authorizationHeader = ihttpSession.getHeaders().get("authorization");
        String contentType = ihttpSession.getHeaders().get("content-type");
        log.debug("validate authorization header");
        if (authorizationHeader != null && authorizationHeader.startsWith("Basic ")) {
            // decode header
            log.debug("decode authorization header");
            byte[] decodedBytes = Base64.decodeBase64(authorizationHeader.replaceFirst("Basic ", ""));
            // and get username and password
            usernamePassword = new String(decodedBytes).split(":");
        } else {
            // otherwise throw forbidden error
            return new ForbiddenHandler().get(uriResource, map, ihttpSession);
        }
        try {
            // get certificates
            SignerAppConfiguration configuration = uriResource.initParameter(SignerAppConfiguration.class);
            if (configuration.keyStore == null) {
                return new ServerErrorHandler("Invalid application configuration. Service keystore cannot be empty at this context").get(uriResource, map, ihttpSession);
            }
            DocuSignSignerMessage message = new DocuSignSignerMessage();
            message.username = usernamePassword[0];
            message.password = usernamePassword[1];
            DocuSignRequestCodeResult res = new DocuSignSigner(configuration.keyStore).getVerificationCode(message);
            if (res.outcome == 0 || res.outcome == 1) {
                NanoHTTPD.Response finalResponse = new JsonResponseHandler(res).get(uriResource, map, ihttpSession);
                CorsHandler.enable(ihttpSession, finalResponse);
                return finalResponse;
            } else {
                return new ServerErrorHandler(res.description).get(uriResource, map, ihttpSession);
            }
        } catch (Exception e) {
            log.error(ExceptionUtils.getStackTrace(e));
            return new ServerErrorHandler(e).get(uriResource, map, ihttpSession);
        }
    }

    public NanoHTTPD.Response put(RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        return new MethodNotAllowedHandler().get(uriResource, map, ihttpSession);
    }

    public NanoHTTPD.Response get(RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        return new MethodNotAllowedHandler().get(uriResource, map, ihttpSession);
    }

    public NanoHTTPD.Response delete(RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        return new MethodNotAllowedHandler().get(uriResource, map, ihttpSession);
    }

    public NanoHTTPD.Response other(String s, RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        return new CorsHandler().other(s, uriResource, map, ihttpSession);
    }
}
