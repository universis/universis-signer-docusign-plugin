package org.universis.signer.docusign;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfSignatureAppearance;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.security.*;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIUtils;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.openxml4j.opc.PackageAccess;
import org.apache.poi.openxml4j.opc.PackagePart;
import org.apache.poi.openxml4j.opc.PackagePartName;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.GeneralSecurityException;
import java.security.MessageDigest;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Properties;

public class DocuSignSigner {

    URI serviceURI;

    Properties options;

    public DocuSignSigner() {
        this.options = new Properties();
    }

    public DocuSignSigner(Properties options) {
        this.options = new Properties();
    }

    public DocuSignSigner(String serviceURI) {
        this(URI.create(serviceURI));
    }

    public DocuSignSigner(URI serviceURI) {
        this.serviceURI = serviceURI;
    }

    public DocuSignSigner(URI serviceURI, Properties options) {
        this(serviceURI);
        this.options = options;
    }

    /**
     * Gets DocuSign certificate collection for the given user
     * @param message - A message which contains username and password
     * @return A collection of DocuSign remote certificates
     * @throws IOException
     */
    DocuSignCertificateMessageResult getCertificates(DocuSignSignerMessage message) throws IOException {
        // create client
        HttpClient client = HttpClientBuilder.create().build();
        // get sign endpoint
        URI signURI = URIUtils.resolve(this.serviceURI, "/Sign/Api/Certificates");
        // create request
        HttpPost request = new HttpPost(signURI);
        // get message as json string
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        String value = mapper.writeValueAsString(message);
        // set http entity
        HttpEntity stringEntity = new StringEntity(value, ContentType.APPLICATION_JSON);
        request.setEntity(stringEntity);
        // execute request
        HttpResponse response = client.execute(request);
        if (response != null) {
            InputStream in = response.getEntity().getContent(); // get json content
            // get byte array
            byte[] inBytes = IOUtils.toByteArray(in);
            DocuSignCertificateMessageResult res = new DocuSignCertificateMessageResult();
            if (response.getStatusLine().getStatusCode() == 200) {
                // deserialize stream to message result
                // important: use this method because service returns also 200 on error
                DocuSignCertificateMessageResult trySuccess = mapper.readValue(inBytes, DocuSignCertificateMessageResult.class);
                // if success is false
                if (!trySuccess.success) {
                    // deserialize and return error
                    res.success = false;
                    res.error = mapper.readValue(inBytes, DocuSignCertificateErrorResult.class);
                    return res;
                }
                // otherwise set success to true
                res.success = true;
                // and return data
                res.data = mapper.readValue(inBytes, DocuSignCertificateMessageResultData.class);
            } else {
                // deserialize stream to message error result
                res.success = false;
                res.error = mapper.readValue(inBytes, DocuSignCertificateErrorResult.class);
            }
            return res;

        } else {
            // throw exception for empty result
            throw new IOException("Service response cannot be empty at this context");
        }
    }

    DocuSignRequestCodeResult getVerificationCode(DocuSignSignerMessage message) throws IOException {
        // create client
        HttpClient client = HttpClientBuilder.create().build();
        // get sign endpoint
        URI signURI = URIUtils.resolve(this.serviceURI, "/Sign/Api/requestOTP");
        // create request
        HttpPost request = new HttpPost(signURI);
        // get message as json string
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        String value = mapper.writeValueAsString(message);
        // set http entity
        HttpEntity stringEntity = new StringEntity(value, ContentType.APPLICATION_JSON);
        request.setEntity(stringEntity);
        // execute request
        HttpResponse response = client.execute(request);
        if (response != null) {
            InputStream in = response.getEntity().getContent(); // get json content
            // get byte array
            byte[] inBytes = IOUtils.toByteArray(in);
            if (response.getStatusLine().getStatusCode() == 200) {
                // deserialize stream to message result
                // important: use this method because service returns also 200 on error
                return mapper.readValue(inBytes, DocuSignRequestCodeResult.class);
            } else {
                // deserialize stream to message error result
                return new DocuSignRequestCodeResult() {
                    {
                        outcome = response.getStatusLine().getStatusCode();
                        description = response.getStatusLine().getReasonPhrase();
                    }
                };
            }

        } else {
            // throw exception for empty result
            throw new IOException("Service response cannot be empty at this context");
        }
    }


    DocuSignSignBufferResult signBuffer(DocuSignBufferMessage message) throws IOException {
        // create client
        HttpClient client = HttpClientBuilder.create().build();
        // get sign endpoint
        URI signURI = URIUtils.resolve(this.serviceURI, "/Sign/Api/SignBuffer");
        // create request
        HttpPost request = new HttpPost(signURI);
        // get message as json string
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        String value = mapper.writeValueAsString(message);
        // set http entity
        HttpEntity stringEntity = new StringEntity(value, ContentType.APPLICATION_JSON);
        request.setEntity(stringEntity);
        // execute request
        HttpResponse response = client.execute(request);
        if (response != null) {
            InputStream in = response.getEntity().getContent(); // get json content
            // get byte array
            byte[] inBytes = IOUtils.toByteArray(in);
            DocuSignSignBufferResult res = new DocuSignSignBufferResult();
            if (response.getStatusLine().getStatusCode() == 200) {
                // deserialize stream to message result
                // important: use this method because service returns also 200 on error
                DocuSignSignBufferResult trySuccess = mapper.readValue(inBytes, DocuSignSignBufferResult.class);
                // if success is false
                if (!trySuccess.success) {
                    // deserialize and return error
                    res.success = false;
                    res.error = mapper.readValue(inBytes, DocuSignSignBufferErrorResult.class);
                    return res;
                }
                res = trySuccess;
            } else {
                // deserialize stream to message error result
                res.success = false;
                res.error = mapper.readValue(inBytes, DocuSignSignBufferErrorResult.class);
            }
            return res;

        } else {
            // throw exception for empty result
            throw new IOException("Service response cannot be empty at this context");
        }
    }

    public URI getServiceURI() {
        return serviceURI;
    }

    public void setServiceURI(URI serviceURI) {
        this.serviceURI = serviceURI;
    }

    public static String tryFindSignatureLine(String file) throws InvalidFormatException, ParserConfigurationException, IOException, SAXException {
        OPCPackage xlsx = OPCPackage.open(file, PackageAccess.READ);
        String signatureLineID = null;
        ArrayList<PackagePart> parts = xlsx.getParts();
        for (PackagePart packagePart : parts) {
            if (packagePart.getContentType().equals("application/vnd.openxmlformats-officedocument.vmlDrawing")) {
                PackagePartName name = packagePart.getPartName();
                String text = IOUtils.toString(packagePart.getInputStream(), StandardCharsets.UTF_8.name());
                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                DocumentBuilder builder = factory.newDocumentBuilder();
                InputSource is = new InputSource(new StringReader(text));
                Document doc = builder.parse(is);
                NodeList nodes = doc.getElementsByTagName("xml");
                if (nodes.getLength() > 0) {
                    // get root element
                    Element rootElement = (Element) nodes.item(0);
                    NodeList shapes = rootElement.getElementsByTagName("v:shape");
                    if (shapes.getLength() > 0) {
                        Element shape = (Element) shapes.item(0);
                        NodeList signatureLines = shape.getElementsByTagName("o:signatureline");
                        if (signatureLines.getLength() > 0) {
                            Element signatureLine = (Element) signatureLines.item(0);
                            return signatureLine.getAttribute("id");
                        }
                    }
                }
            }
        }
        return null;
    }

    /**
     * Signs the given input stream which represents a pdf document
     */
    DocuSignSignerMessageResult sign(DocuSignSignerMessage message,
                                   String timestampServer,
                                   String image) throws IOException, DocumentException, GeneralSecurityException {
        // create reader
        byte[] inputBuffer = Base64.getDecoder().decode(message.fileData);
        PdfReader reader = new PdfReader(new ByteArrayInputStream(inputBuffer));
        String outFile = File.createTempFile("signPDF",".pdf").getAbsolutePath();
        OutputStream outputStream = Files.newOutputStream(Paths.get(outFile));
        // create signature
        PdfStamper stamper = PdfStamper.createSignature(reader, outputStream, '\0', null, true);
        // get signature appearance
        PdfSignatureAppearance appearance = stamper.getSignatureAppearance();
        if (message.reason != null) {
            appearance.setReason(message.reason);
        }
        Font font = new Font(Font.FontFamily.HELVETICA, 10);
        appearance.setLayer2Font(font);

        // set signature position
        Rectangle finalPosition = new Rectangle(50, 10, 290, 100);
        if (message.width != null) {
            finalPosition = new Rectangle(message.x, message.y, message.width, message.height);
        }
        // add image
        if (image != null) {
            Image imageInstance = Image.getInstance(image);
            appearance.setSignatureGraphic(imageInstance);
            appearance.setRenderingMode(PdfSignatureAppearance.RenderingMode.GRAPHIC_AND_DESCRIPTION);
        }
        appearance.setVisibleSignature(finalPosition, message.page, message.signatureFieldName == null ? "sig": message.signatureFieldName);

        DocuSignCertificateMessageResult result = this.getCertificates(new DocuSignSignerMessage() {
            {
                username = message.username;
                password = message.password;
            }
        });
        if (!result.success) {
            throw new IOException(result.error.message);
        }
        if (result.data.certificates.isEmpty()) {
            throw new GeneralSecurityException("A valid certificate cannot be found");
        }
        // get cert
        List<Certificate> certificates = new ArrayList<Certificate>();
        CertificateFactory certFactory = CertificateFactory.getInstance("X.509");
        for (String certificate:result.data.certificates) {
            InputStream in = new ByteArrayInputStream(Base64.getDecoder().decode(certificate));
            certificates.add((X509Certificate)certFactory.generateCertificate(in));
        }
        Certificate[] chain = new Certificate[certificates.size() - 1];
        chain = certificates.toArray(chain);
        // create hash
        MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
        byte[] messageInput = Base64.getDecoder().decode(message.fileData);
        byte[] messageHash = DigestAlgorithms.digest(new ByteArrayInputStream(messageInput), messageDigest);
        String messageBuffer = Base64.getEncoder().encodeToString(messageHash);

        DocuSignBufferMessage signBufferMessage = new DocuSignBufferMessage() {
            {
                username = message.username;
                password = message.password;
                otp = message.otp;
                buffer = new ArrayList<String>() {
                    {
                        add(messageBuffer);
                    }
                };
            }
        };

        // create signature
        DocuSignRemoteSignature signature = new DocuSignRemoteSignature(this, message.username, message.password, message.otp);

        BouncyCastleDigest digest = new BouncyCastleDigest();
        TSAClientBouncyCastle tsaClient = null;
        if (timestampServer != null) {
            tsaClient = new TSAClientBouncyCastle(timestampServer, "", "");
        }
        // and finally sign pdf
        List<CrlClient> crlList = new ArrayList<CrlClient>();
        crlList.add(new CrlClientOnline(chain));
        // OcspClient ocspClient = new OcspClientBouncyCastle(null);
        MakeSignature.signDetached(appearance, digest, signature, chain, crlList, null, tsaClient, this.getSignatureEstimatedSize(), MakeSignature.CryptoStandard.CADES);
        outputStream.close();
        // get outfile
        byte[] outBuffer = FileUtils.readFileToByteArray( new File(outFile));
        return new DocuSignSignerMessageResult() {
            {
                success = true;
                data = new DocuSignSignerMessageResultData() {{
                    signedFileData = Base64.getEncoder().encodeToString(outBuffer);
                }};
            }
        };

    }

    int getSignatureEstimatedSize() {
        if (this.options == null) {
            return 0;
        }
        String strSignatureEstimatedSize = options.getProperty("signatureEstimatedSize");
        if (strSignatureEstimatedSize != null) {
            return Integer.parseInt(strSignatureEstimatedSize);
        }
        return 0;
    }



}
