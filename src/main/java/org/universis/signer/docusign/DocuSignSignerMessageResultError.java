package org.universis.signer.docusign;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DocuSignSignerMessageResultError {

    @JsonProperty("Message")
    public String message;

    @JsonProperty("InnerCode")
    public int innerCode;

    @JsonProperty("Code")
    public int code;

    @JsonProperty("Module")
    public String module;
}
