package org.universis.signer.docusign;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DocuSignCertificateMessageResult {
    @JsonProperty("Success")
    public Boolean success = true;

    @JsonProperty("Data")
    public DocuSignCertificateMessageResultData data;

    @JsonProperty("ErrData")
    public DocuSignCertificateErrorResult error;
}
