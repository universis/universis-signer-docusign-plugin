package org.universis.signer.docusign;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DocuSignSignBufferErrorResult {
    @JsonProperty("Message")
    public String message;

    @JsonProperty("InnerCode")
    public int innerCode;

    @JsonProperty("Code")
    public int code;

    @JsonProperty("Module")
    public String module;
}
