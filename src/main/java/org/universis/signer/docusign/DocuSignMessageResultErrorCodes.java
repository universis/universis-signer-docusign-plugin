package org.universis.signer.docusign;

public class DocuSignMessageResultErrorCodes {
    final static int LogonFailure = 38374;
    final static int RequiredField = 38628;
}
