package org.universis.signer.docusign;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DocuSignSignBufferResult {
    @JsonProperty("Success")
    public Boolean success;

    @JsonProperty("Data")
    public DocuSignSignBufferResultData data;

    @JsonProperty("ErrData")
    public DocuSignSignBufferErrorResult error;
}
